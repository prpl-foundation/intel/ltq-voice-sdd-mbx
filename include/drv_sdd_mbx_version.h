#ifndef _DRV_SDD_MBX_VERSION_H
#define _DRV_SDD_MBX_VERSION_H
/*******************************************************************************

  Copyright 2014-2015 Lantiq Deutschland GmbH
  Copyright 2015-2016 Lantiq Beteiligungs-GmbH & Co.KG
  Copyright 2016-2017 Intel Corporation.

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

*******************************************************************************/

/**
   \file drv_sdd_mbx_version.h
   This file contains the version information of SDD mailbox driver.
*/

/* ========================================================================== */
/*                            Macro definitions                               */
/* ========================================================================== */
#define MAJORSTEP    1
#define MINORSTEP    4
#define VERSIONSTEP  0
#define VERS_TYPE    0

#define SDD_MBX_INFO       "SDD mailbox driver"
#define SDD_MBX_COPYRIGHT  "(c) 2014-2017 Intel Corporation"

#endif /* _DRV_SDD_MBX_VERSION_H */
