#ifndef _DRV_SDD_MBX_LINUX_H
#define _DRV_SDD_MBX_LINUX_H
/******************************************************************************

                            Copyright (c) 2014-2015
                        Lantiq Beteiligungs-GmbH & Co.KG
                             http://www.lantiq.com

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

 ******************************************************************************/

/**
   \file drv_sdd_mbx_linux.h
   This file contains the implementation SDD mailbox Linux specific functions.
 */

/* ========================================================================== */
/*                                 Includes                                   */
/* ========================================================================== */
#include "ifx_types.h"

#endif /* _DRV_SDD_MBX_LINUX_H */
